package RassolovDispatcher;
use strict;
use Module::RassolovApp;
require './Module/io_cgi.pl';

# Простой конструктор
sub new
{
  my $this = shift;
  my $self = {};
  return bless ($self, $this);
}

# Метод запуска диспетчера и обработка параметров
sub run
{
  my $app = RassolovApp->new();
  my $io_cgi = 'io_cgi'->new();
  $io_cgi->get_params();

  # Из сылки получаем имя класса, запускаемый метод
  my $class = $io_cgi->param('class');
  my $method = $io_cgi->param('method');

  eval
  {
    # Соединение с бд
    $app->connect_bd();

    # Подключаем модуль (контроллер) нужного класса и создаем его объект
    require "./Controller/$class.pm";

    my $object = $class->new();

    # Вызываем метод полученного класса
    $object->$method();

    # Закрываем бд
    $app->disconnect_bd();
  };
  if ($@)
  {
    # Выводим ошибки если есть
    print "Content-Type: text/html\n\n";
    print $@;
  }
}

1;
