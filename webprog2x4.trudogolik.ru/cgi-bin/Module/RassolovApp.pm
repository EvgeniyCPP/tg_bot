package RassolovApp;
use strict;
use DBI;

# Глобальная переменная для хранения объекта базы данных
our $single_db_object = undef;

sub new
{
  my $this = shift;

  # Если объект уже есть возвращаем его
  return $single_db_object if defined $single_db_object;

  # Иначе создаем новый и возвращаем его
  my $self = {};
  $single_db_object = bless ($self, $this);
  $self->{prefix_bd}   = "webprog2x4_";
  $self->{attr}        = {PrintError => 0, RaiseError => 0};
  $self->{data_source} = "DBI:mysql:".$self->{prefix_bd}."tgbot:95.213.197.229";
  $self->{username}    = $self->{prefix_bd}."tgbot";
  $self->{password}    = "0iTDdcgpiO0rCu4B";
  $self->{encode}      = "cp1251";
  $self->{connection}  = undef;

  return $single_db_object;
}

# Возвращаем instance бд
sub get_instance_bd
{
  my $this = shift;
  $this->new() if !defined ($single_db_object);
  return $single_db_object;
}

# Метод создания соединения с бд записываем дескриптор подключения в хеш параметров
sub connect_bd
{
  my $this = shift;
  if (!defined $this->{connection})
  {
    my $db_connect = DBI->connect($this->{data_source}, $this->{username}, $this->{password}, $this->{atr});
    die $DBI::errstr if !$db_connect;
    $db_connect->do("SET NAMES $this->{encode}");
    $this->{connection} = $db_connect;
  }
  else
  {
    $this->{connection} = undef;
  }
}

# Возвращаем префикс базы данных
sub get_prefix_bd
{
  my $this = shift;
  return $this->{prefix_bd};
}

# Возвращаем дескриптор подключения к бд
sub get_connect_bd
{
  my $this = shift;
  return $this->{connection};
}

# Метод удаления соединения с бд записываем дескриптор подключения в хеш параметров как undef
sub disconnect_bd
{
  my $this = shift;
  $this->{connection}->disconnect();
  $this->{connection} = undef;
}
1;
