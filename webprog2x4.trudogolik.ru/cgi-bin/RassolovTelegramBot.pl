use strict;
use DBI;
use Encode;

use lib '.';
use Module::WebProgTelegramClient;
use Module::RassolovApp;

# Длина deadline в днях
my $length_deadline = 4;

# Данные для бота
my $token = '1825843888:AAF7sh0SmeJ1bK6Q4fyWH8PngKRQUfg4vzA';
my ($offset, $updates) = 0;

# Объект бота
my $tg_bot = WebProgTelegramClient->new(token => $token);

# Подключение к БД
my $app = RassolovApp->new();
$app->connect_bd();
my $dbh = $app->{connection};

# Бесконечный цикл бота
while (1)
{
  $updates = $tg_bot->call('getUpdates', {
    timeout => 30,
    $offset ? (offset => $offset) : (),
  });

  unless ($updates and ref $updates eq 'HASH' and $updates->{ok})
  {
    warn 'Ошибка!!!';
    next;
  }

  # Перебираем update чата
  for my $item_update ( @{$updates->{result}} )
  {
    $offset = $item_update->{update_id} + 1 if $item_update->{update_id} >= $offset;

    # Если ктото написал сообщение в группе
    if ($item_update->{message} && $item_update->{message}->{chat}->{type} eq 'group')
    {
      my $chat_id = $item_update->{message}->{chat}->{id};

      # Записываем в бд группу
      add_group_in_bd($item_update->{message}->{chat});

      # Получение пользователей
      my $leave_user = $item_update->{message}->{left_chat_member};
      my $new_user = $item_update->{message}->{new_chat_member};
      my $message_from_user = $item_update->{message}->{from};

      # Если есть прибывшие или выбывшие или написавшие письмо в чате.
      if ($leave_user)
      {
        $tg_bot->call('sendMessage', {
          chat_id => $chat_id, text=> decode('utf-8', "До скорой встречи").", $leave_user->{first_name}!"});
      }
      elsif ($new_user)
      {
        add_user_in_bd($new_user);
        add_users_groups_in_bd($new_user->{id}, $chat_id);

        $tg_bot->call('sendMessage', {
          chat_id => $chat_id,
          text=> decode('utf-8', "Добро пожаловать").", $new_user->{first_name}!"
        });
      }
      elsif ($message_from_user)
      {
        add_user_in_bd($message_from_user);
        add_users_groups_in_bd($message_from_user->{id}, $chat_id);

        # Определение хештега админа дз и даты
        if(is_admin($chat_id, $message_from_user->{id})
          && encode('utf-8', $item_update->{message}->{text})
          =~ qq /#домашнеезадание(\\d+) (\\d{2})\\.(\\d{2})\\.(\\d{4})[\\s\\S]*/)
        {
          add_homework_in_bd($1, {day => $2, moth => $3, year => $4});
        }
        if(!is_admin($chat_id, $message_from_user->{id})
          && encode('utf-8', $item_update->{message}->{text}) =~ qq /#дз(\\d+)[\\s\\S]*/)
        {
          if(add_handed_homework_in_bd($1, $item_update->{message}->{date}, $message_from_user->{id}))
          {
            $tg_bot->call('sendMessage', {
              chat_id => $chat_id,
              text => "$message_from_user->{first_name}, ".decode('utf-8', "поздравляю, вы уложились в deadline!"),
              reply_to_message_id => $item_update->{message}->{message_id},
            });
          }
          else
          {
            $tg_bot->call('sendMessage', {
              chat_id => $chat_id,
              text => "$message_from_user->{first_name}, ".decode('utf-8', "к сожалению вы не уложились в deadline!"),
              reply_to_message_id => $item_update->{message}->{message_id},
            });
          }
        }
      }
    }
  }
}

# Метод выполнения скрипта select принимает sql вида строку и возвращает ссылку на массив результат выборки
sub select_query
{
  my $sql = shift;
  my $sth = $dbh->prepare($sql);
  $sth->execute() or die "SQL Error: $DBI::errstr\n";
  my $res = $sth->fetch();
  $sth->finish();
  return $res;
}

# Добавление сданной задачи в базу на вход номер дз, дата сдачи и user_id.
# Проверка на дедлайн задачи если уложился максимальная оценка.
# Если уложился в деадлайн возвращает 1 иначе 0.
sub add_handed_homework_in_bd
{
  my $number_homework = shift;
  my $handed_date = shift;
  my $user_id = shift;
  my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime ($handed_date);
  $year += 1900;
  $mon += 1;
  $mon = $mon < 10 ? "0".$mon : $mon;
  my $res = select_query("SELECT * FROM webprog2x4_handed_homework
    WHERE webprog2x4_homework_number = $number_homework
    AND webprog2x4_user_tg_user_id = $user_id");

  # Получаем ссылку на массив с max_score и deadline текущей задачи
  my $score_deadline = select_query("SELECT max_score, deadline FROM webprog2x4_homework
    WHERE number = $number_homework");
  my $score = $score_deadline->[1] lt "$year-$mon-$mday" ? 0 : $score_deadline->[0];

  #Если дз не сдано добавляем, иначе обновляем
  if (!defined($res) && !$res)
  {
    my $sql = "INSERT INTO webprog2x4_handed_homework SET webprog2x4_homework_number = ?,
      date_for_assignment = ?, score = ?, webprog2x4_user_tg_user_id = ?";
    my $sth = $dbh->prepare($sql);
    $sth->bind_param(1, $number_homework);
    $sth->bind_param(2, "$year-$mon-$mday");
    $sth->bind_param(3, $score);
    $sth->bind_param(4, $user_id);
    $sth->execute();
    $sth->finish();
  }
  $score_deadline->[1] lt "$year-$mon-$mday" ? return 0 : return 1;
}

# Добавляем группу если ее нет в бд принимаем ссылку на хеш чата.
sub add_group_in_bd
{
  my $group_chat = shift;
  my $res = select_query("SELECT * FROM webprog2x4_group_tg WHERE tg_chat_id = $group_chat->{id}");

  #Если группы нет добавляем иначе обновляем
  if (!defined($res) && !$res)
  {
    my $sql = "INSERT INTO webprog2x4_group_tg SET title = ?, tg_chat_id = ?";
    my @values = (encode("cp1251", $group_chat->{title}) || ' ', $group_chat->{id});
    my $sth = $dbh->prepare($sql);
    $sth->execute(@values);
    $sth->finish();
  }
  else
  {
    my $sql = "UPDATE webprog2x4_group_tg SET title = ? WHERE tg_chat_id = ?";
    my $sth = $dbh->prepare($sql);
    $sth->bind_param(1, encode("cp1251", $group_chat->{title}) || ' ');
    $sth->bind_param(2, $group_chat->{id});
    $sth->execute();
    $sth->finish();
  }
}

# Добавляем дз + к дате прибавляем время для deadline в бд если его нет,
# на вход получаем номер дз и дату в виде ссылки хеша {day = > ??, math => ??, year => ??}
sub add_homework_in_bd
{
  my $num_homework = shift;
  my $deadline = shift;
  my $res = select_query("SELECT * FROM webprog2x4_homework WHERE number = $num_homework");

  # Если дз нет с текущим данными добавляем новое
  if (!defined($res) && !$res)
  {
    my $sql = "INSERT INTO webprog2x4_homework SET deadline = DATE_ADD(?, INTERVAL $length_deadline DAY), number = ?";
    my $sth = $dbh->prepare($sql);
    $sth->bind_param(1, "$deadline->{year}-$deadline->{moth}-$deadline->{day}");
    $sth->bind_param(2, $num_homework);
    $sth->execute();
    $sth->finish();
  }
}

# Связывание таблиц users и groups по telegram id
sub add_users_groups_in_bd
{
  my $user_id = shift;
  my $chat_id = shift;
  my $res = select_query("SELECT * FROM webprog2x4_group_by_user
    WHERE webprog2x4_user_id = $user_id AND webprog2x4_group_tg_id = $chat_id");

  # Если данных нет добавляем
  if (!defined($res) && !$res)
  {
    my $sql = "INSERT INTO webprog2x4_group_by_user SET webprog2x4_user_id = ?, webprog2x4_group_tg_id =?";
    my @values = ($user_id, $chat_id);
    my $sth = $dbh->prepare($sql);
    $sth->execute(@values);
    $sth->finish();
  }

}

# Добавляем юзера в бд если его нет, на вход получаем ссылку на хеш с данными юзера из бота
# иначе обновляем юзера
sub add_user_in_bd
{
  my $user = shift;
  my $res = select_query("SELECT * FROM webprog2x4_user WHERE tg_user_id = $user->{id}");

  # Если юзера нет с текущим id добавляем
  if (!defined($res) && !$res)
  {
    my $sql = "INSERT INTO webprog2x4_user SET first_name = ?, last_name = ?, tg_account_name = ?, tg_user_id =?";
    my @values = (encode("cp1251", $user->{first_name}) || ' ', encode("cp1251", $user->{last_name}) || ' ',
      encode("cp1251", $user->{username}) || ' ', $user->{id});
    my $sth = $dbh->prepare($sql);
    $sth->execute(@values);
    $sth->finish();
  }
  else
  {
    my $sql = "UPDATE webprog2x4_user SET first_name = ?, last_name = ?, tg_account_name = ? WHERE tg_user_id = ?";
    my $sth = $dbh->prepare($sql);
    $sth->bind_param(1, encode("cp1251", $user->{first_name}) || ' ');
    $sth->bind_param(2, encode("cp1251", $user->{last_name}) || ' ');
    $sth->bind_param(3, encode("cp1251", $user->{username}) || ' ');
    $sth->bind_param(4, $user->{id});
    $sth->execute();
    $sth->finish();
  }
}

#Проверка юзера на админа, если админ возвращает 1 иначе 0. На вход chat_id, user_id
sub is_admin
{
  my $chat_id_sub = shift;
  my $user_id = shift;
  for my $admin_user ( @{$tg_bot->call('getChatAdministrators', {chat_id => $chat_id_sub})->{result}} )
  {
    return 1 if $user_id == $admin_user->{user}->{id};
  }
  return 0;
}

1;
