package RassolovGroup;
use strict;
use HTML::Template;
use Model::RassolovGroup;

# Простой конструктор
sub new
{
  my $this = shift;
  my $self = {};
  return bless ($self, $this);
}

# Метод получения списка групп из модели и вывода его методом _view_groups
sub show_list
{
  my $this = shift;
  my $group_model = Model::RassolovGroup->new();
  $this->_view_groups($group_model->get_all());
}

# Метод удаления группы по полученному id из модели групп и выводом списка групп на экран методом _view_groups
sub delete
{
  my $this = shift;
  my $group_model  = Model::RassolovGroup->new();
  $group_model->delete_group_by_id();
  $this->_view_groups($group_model->get_all());
}

# Метод добавления группы в модель групп и выводом списка групп на экран методом _view_group
sub add
{
  my $this = shift;
  my $group_model  = Model::RassolovGroup->new();
  $group_model->add_group();
  $this->_view_groups($group_model->get_all());
}

# Метод вывода полученного списка на экран через шаблон, в качестве параметра список групп в виде массива хешей
sub _view_groups
{
  my $this     = shift;
  my $groups   = shift;
  my $template = HTML::Template->new(filename => './Template/rassolov_group_list.html');
  $template->param(
    groups_loop => $groups
  );
  print "Content-Type: text/html\n\n";
  print $template->output();
}
1;
