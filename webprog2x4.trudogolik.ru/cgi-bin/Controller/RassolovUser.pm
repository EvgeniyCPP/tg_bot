package RassolovUser;
use strict;
use HTML::Template;
use Model::RassolovUser;

# ������� �����������
sub new
{
  my $this = shift;
  my $self = {};
  return bless ($self, $this);
}

# ����� ������ ������������� ������ �� �����
sub show_users_by_group
{
  my $this = shift;
  my $group_params = shift;
  my $user_model = Model::RassolovUser->new();
  $this->_view_users_by_group($user_model->get_users_in_group_by_id());
}

# ����� ������ ����������� ������ �� ����� ����� ������, � �������� ��������� ������ ������������� � ���� ������� �����
sub _view_users_by_group
{
  my $this = shift;
  my $users = shift;
  my $template = HTML::Template->new(filename => './Template/rassolov_user_list_by_group.html');
  $template->param(
    users_loop => $users
  );
  print "Content-Type: text/html\n\n";
  print $template->output();
}
1;
