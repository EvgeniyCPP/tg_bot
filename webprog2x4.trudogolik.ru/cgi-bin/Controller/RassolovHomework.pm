package RassolovHomework;
use strict;
use HTML::Template;
use Model::RassolovHomework;

# Простой конструктор
sub new
{
  my $this = shift;
  my $self = {};
  return bless ($self, $this);
}

# Метод вывода домашних работ группы на экран
sub show_homeworks_by_group
{
  my $this           = shift;
  my $homework_model = Model::RassolovHomework->new();
  $this->_view_homeworks_by_group($homework_model->get_homework_in_group_by_id());
}

# Метод вывода сданных домашних работ группы на экран
sub show_result_by_group
{
  my $this           = shift;
  my $homework_model = Model::RassolovHomework->new();
  $this->_view_result_by_group($homework_model->get_handed_homework_in_group_by_id());
}

# Метод вывода полученного списка на экран через шаблон,
# в качестве параметра список домашних работ в виде массива хешей
sub _view_homeworks_by_group
{
  my $this      = shift;
  my $homeworks = shift;
  my $template  = HTML::Template->new(filename => './Template/rassolov_homework_list_by_group.html');
  $template->param(
    homework_loop => $homeworks
  );
  print "Content-Type: text/html\n\n";
  print $template->output();
}

# Метод вывода полученного списка на экран через шаблон,
# в качестве параметра список сданных домашних работ в виде массива хешей
sub _view_result_by_group
{
  my $this             = shift;
  my $handed_homeworks = shift;
  my $template         = HTML::Template->new(filename => './Template/rassolov_results_list_by_group.html');
  $template->param(
    results_loop => $handed_homeworks
  );
  print "Content-Type: text/html\n\n";
  print $template->output();
}
1;
