package Model::RassolovGroup;
use strict;
use Module::RassolovApp;
require './Module/io_cgi.pl';

# Конструктор
sub new
{
  my $this = shift;
  my $self = {
    db_connect => RassolovApp->get_instance_bd()->get_connect_bd(),
    prefix_bd  => RassolovApp->get_instance_bd()->get_prefix_bd(),
    io_cgi     => 'io_cgi'->get_instance_cgi(),
  };
  return bless ($self, $this);
}

# Метод получения списка групп из базы и возрат этого списка в виде массива хешей
sub get_all
{
  my $this = shift;
  $this->{io_cgi}->get_params();
  my $db_connect = $this->{db_connect};
  my $tbl_group_tg = $this->{prefix_bd}."group_tg";
  my $sql = "SELECT id, title, tg_chat_id FROM $tbl_group_tg ORDER BY id ASC";
  my $sth = $db_connect->prepare($sql);
  $sth->execute() or die "SQL Error: $DBI::errstr\n";
  my $groups = [];
  while (my $row = $sth->fetchrow_hashref)
  {
    push(@$groups, $row);
  }
  $sth->finish();
  return $groups;
}

# Метод добавления группы в базу данных на входе get параметры title и tg_chat_id
sub add_group
{
  my $this = shift;
  my $db_connect = $this->{db_connect};
  my $tbl_group_tg = $this->{prefix_bd}."group_tg";
  my $sql = "INSERT INTO $tbl_group_tg  SET title=?, tg_chat_id=?";
  my $sth = $db_connect->prepare($sql);
  $sth->execute($this->{io_cgi}->param('title'),
    $this->{io_cgi}->param('tg_chat_id')) or die "SQL Error: $DBI::errstr\n";
  $sth->finish();
}

# Метод удаления группы из бд по номеру id на вход функции получаем get параметр id
sub delete_group_by_id
{
  my $this = shift;
  my $group_id = $this->{io_cgi}->param('id');
  my $db_connect = $this->{db_connect};
  my $tbl_group_tg = $this->{prefix_bd}."group_tg";
  my $sql = "DELETE FROM $tbl_group_tg WHERE id = $group_id";
  my $sth = $db_connect->prepare($sql);
  $sth->execute() or die "SQL Error: $DBI::errstr\n";
  $sth->finish();
}

1;
