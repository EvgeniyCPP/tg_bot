package Model::RassolovUser;
use strict;
use Module::RassolovApp;
require './Module/io_cgi.pl';

# Конструктор
sub new
{
  my $this = shift;
  my $self = {
    db_connect => RassolovApp->get_instance_bd()->get_connect_bd(),
    prefix_bd  => RassolovApp->get_instance_bd()->get_prefix_bd(),
    io_cgi     => 'io_cgi'->get_instance_cgi(),
  };
  return bless ($self, $this);
}

# Метод получения списка пользователей группы из бд по номеру telegram_id на вход функции получаем get параметр
# tg_group_id на выходе массив хешей
sub get_users_in_group_by_id
{
  my $this = shift;
  my $tg_group_id = $this->{io_cgi}->param('tg_group_id');
  my $db_connect = $this->{db_connect};
  my $tbl_user = $this->{prefix_bd}."user";
  my $tbl_group_by_user = $this->{prefix_bd}."group_by_user";
  my $sql = "SELECT $tbl_user.id, first_name, last_name, tg_account_name, tg_user_id
              FROM $tbl_user, $tbl_group_by_user
              WHERE $tbl_group_by_user.group_tg_id = $tg_group_id
                    AND $tbl_group_by_user.user_id = tg_user_id
              ORDER BY id ASC";
  my $sth = $db_connect->prepare($sql);
  $sth->execute() or die "SQL Error: $DBI::errstr\n";
  my $users = [];
  while (my $row = $sth->fetchrow_hashref)
  {
    push(@$users, $row);
  }
  $sth->finish();
  return $users;
}

1;
