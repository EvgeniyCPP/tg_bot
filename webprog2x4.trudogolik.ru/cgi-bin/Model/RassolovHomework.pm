package Model::RassolovHomework;
use strict;
use Module::RassolovApp;
require './Module/io_cgi.pl';

# Конструктор
sub new
{
  my $this = shift;
  my $self = {
    db_connect => RassolovApp->get_instance_bd()->get_connect_bd(),
    prefix_bd  => RassolovApp->get_instance_bd()->get_prefix_bd(),
    io_cgi     => 'io_cgi'->get_instance_cgi(),
  };
  return bless ($self, $this);
}

# Метод получения списка сданных домашних работ группы из бд по номеру telegram_id на вход функции получаем get параметр
# tg_group_id, на выходе массив хешей
sub get_handed_homework_in_group_by_id
{
  my $this = shift;
  my $tg_group_id = $this->{io_cgi}->param('tg_group_id');
  my $tbl_handed_homework = $this->{prefix_bd}."handed_homework";
  my $tbl_group_by_user = $this->{prefix_bd}."group_by_user";
  my $db_connect = $this->{db_connect};
  my $sql = "SELECT $tbl_handed_homework.id, homework_number, date_for_assignment, score, user_tg_user_id
              FROM $tbl_handed_homework
              JOIN $tbl_group_by_user
                ON $tbl_group_by_user.user_id = user_tg_user_id
                  AND $tbl_group_by_user.group_tg_id = $tg_group_id
              ORDER BY id ASC";
  my $sth = $db_connect->prepare($sql);
  $sth->execute() or die "SQL Error: $DBI::errstr\n";
  my $handed_homeworks = [];
  while (my $row = $sth->fetchrow_hashref)
  {
    push(@$handed_homeworks, $row);
  }
  $sth->finish();
  return $handed_homeworks;
}

# Метод получения списка домашних работ группы из бд по номеру telegram_id на вход функции получаем get параметр
# tg_group_id, на выходе массив хешей
sub get_homework_in_group_by_id
{
  my $this = shift;
  my $tg_group_id = $this->{io_cgi}->param('tg_group_id');
  my $db_connect = $this->{db_connect};
  my $tbl_homework = $this->{prefix_bd}."homework";
  my $sql = "SELECT id, max_score, deadline, number, group_tg_id
      FROM $tbl_homework
      WHERE group_tg_id = $tg_group_id
      ORDER BY id ASC";
  my $sth = $db_connect->prepare($sql);
  $sth->execute() or die "SQL Error: $DBI::errstr\n";
  my $homeworks = [];
  while (my $row = $sth->fetchrow_hashref)
  {
    push(@$homeworks, $row);
  }
  $sth->finish();
  return $homeworks;
}

1;
